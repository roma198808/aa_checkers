require 'colorize'

class Board
  attr_accessor :rows

  def initialize
    @rows = put_pieces_on_board
  end

  # renders board's state nicely for the user
  def display
    puts
    (1..10).to_a.reverse.each do |num|
      num.even? ? (color_mix = "default") : (color_mix = "cyan")
      print "#{num.to_s.ljust(2," ")}"
      ('a'..'j').each do |letter|

        if @rows[letter+num.to_s].nil?
          (print "   "
                   .colorize(:background => color_mix.to_sym))
        else
          (print " #{@rows[letter+num.to_s].icon} "
                   .colorize(:background => color_mix.to_sym))
        end

        color_mix == "default" ? (color_mix = "cyan") : (color_mix = "default")
      end
      puts
    end
    print "   a  b  c  d  e  f  g  h  i  j"
    puts
    puts
  end


  private

  # creates an empty board
  def create_board
    board = {}

    (1..10).to_a.reverse.each do |num|
      ('a'..'j').each do |letter|
        board[letter+num.to_s] = nil
      end
    end

    board
  end

  # creates initial setup
  def put_pieces_on_board
    board = create_board

    board["a1"] = Piece.new("white")
    board["c1"] = Piece.new("white")
    board["e1"] = Piece.new("white")
    board["g1"] = Piece.new("white")
    board["i1"] = Piece.new("white")
    board["b2"] = Piece.new("white")
    board["d2"] = Piece.new("white")
    board["f2"] = Piece.new("white")
    board["h2"] = Piece.new("white")
    board["j2"] = Piece.new("white")

    board["a3"] = Piece.new("white")
    board["c3"] = Piece.new("white")
    board["e3"] = Piece.new("white")
    board["g3"] = Piece.new("white")
    board["i3"] = Piece.new("white")
    board["b4"] = Piece.new("white")
    board["d4"] = Piece.new("white")
    board["f4"] = Piece.new("white")
    board["h4"] = Piece.new("white")
    board["j4"] = Piece.new("white")


    board["a7"]  = Piece.new("black")
    board["c7"]  = Piece.new("black")
    board["e7"]  = Piece.new("black")
    board["g7"]  = Piece.new("black")
    board["i7"]  = Piece.new("black")
    board["b8"] = Piece.new("black")
    board["d8"] = Piece.new("black")
    board["f8"] = Piece.new("black")
    board["h8"] = Piece.new("black")
    board["j8"] = Piece.new("black")

    board["a9"]  = Piece.new("black")
    board["c9"]  = Piece.new("black")
    board["e9"]  = Piece.new("black")
    board["g9"]  = Piece.new("black")
    board["i9"]  = Piece.new("black")
    board["b10"] = Piece.new("black")
    board["d10"] = Piece.new("black")
    board["f10"] = Piece.new("black")
    board["h10"] = Piece.new("black")
    board["j10"] = Piece.new("black")

    board
  end

end
