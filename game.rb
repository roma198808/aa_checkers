require_relative 'board'
require_relative 'piece'

class Game

  def initialize
    @board_o   = Board.new
    @color     = "white"
    @from, @to = nil
  end

  def run
    until game_over?
      @board_o.display

      begin
        get_user_input

        piece_o = @board_o.rows[@from]
        piece_o.perform_moves!(@from, @to, @board_o)
      rescue StandardError => e
        puts
        puts "Problem: #{e.message}"
        retry
      end

      @color = ( @color == "white" ) ? "black" : "white"
    end

    puts
    puts "#{@color.capitalize}: Game Over!"
    @board_o.display
  end



  private

  # sets @from and @to variables
  def get_user_input
    puts "#{@color.capitalize} move"
    puts

    print "From: "
    @from = gets.chomp

    print "To  : "
    @to = gets.chomp
    @to = @to.split(',')

    if @board_o.rows[@from].nil? ||
       @board_o.rows[@from].color != @color
       raise "Please, select correct checker"
    end

    nil
  end

  # return true if at least one color doesn't exist
  def game_over?
    @board_o.rows.values.compact.none? { |obj| obj.color == "white" } ||
    @board_o.rows.values.compact.none? { |obj| obj.color == "black" }
  end

end


if __FILE__ == $PROGRAM_NAME
  game = Game.new
  game.run
end